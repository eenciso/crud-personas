<?php

use Models\Personas;

require '../../includes/app.php';

$id = $_GET['id'];

$generos = Personas::getGeneros();
$nacionalidades = Personas::getNacionalidades();
$estado_civil = Personas::getEstadoCivil();
$companias = Personas::getCompanias();
$profesiones = Personas::getProfesiones();
$idiomas = Personas::getIdiomas();

// Se obtienen los datos de la persona que ya están instanciados en la clase
$persona = Personas::find($id);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Se obtienen los datos modificados
    $nuevos_datos = $_POST['persona'];

    // Se sincronizan los nuevos datos con los del objeto en memoria
    $persona->sincronizarDatos($nuevos_datos);

    $errores = $persona->validarPersona();

    if (empty($errores)) {
		$persona->update();
    }
}

layout('header');
?>

<div class="contenedor">
	<h2>Actualizar Alumno</h2>
	<form method="post" class="formulario formulario-alumno" novalidate>

        <?php include 'formulario.php' ?>

		<div class="flex justify-right">
			<button type="submit" class="btn btn--submit">Actualizar</button>
		</div>
	</form>
</div>

<?php
layout('footer');
?>
