<?php

use Models\Personas;

require '../../includes/app.php';

$status = null;

$generos = Personas::getGeneros();
$nacionalidades = Personas::getNacionalidades();
$estado_civil = Personas::getEstadoCivil();
$companias = Personas::getCompanias();
$profesiones = Personas::getProfesiones();
$idiomas = Personas::getIdiomas();

$persona = new Personas();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $datos_persona = $_POST['persona'];
    $persona = new Personas($datos_persona);
    $errores = $persona->validarPersona();

    if (empty($errores)) {
        $persona->save();
    }

}

layout('header');
?>

<div class="contenedor">
	<h2>Crear Persona</h2>
    <?php if (!is_null($status)): ?>
		<div class="alerta alerta-exito">Mensaje</div>
    <?php endif; ?>
	<form method="post" class="formulario formulario-alumno" novalidate>

        <?php include 'formulario.php' ?>

		<div class="flex justify-right">
			<button type="submit" class="btn btn--submit">Guardar</button>
		</div>
	</form>
</div>

<?php
layout('footer');
?>
