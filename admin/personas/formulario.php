<div class="flex justify-right">
	<a href="/" class="cerrar-formulario">&times;</a>
</div>
<p class="titulo-formulario">Datos</p>
<div class="layout-formulario">
	<div class="columna-formulario">
		<div class="campo-formulario">
			<label for="nombre">Nombre:</label>
			<input type="text" name="persona[nombre]" id="nombre" placeholder="Nombre(s)" value="<?php echo $persona->nombre; ?>" required>
            <?php if (isset($errores['nombre'])): ?>
				<p class="error"><?php echo $errores['nombre'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="ap_pat">Apellido Paterno:</label>
			<input type="text" name="persona[apellido_paterno]" id="ap_pat" placeholder="Primer apellido" value="<?php echo $persona->apellido_paterno; ?>" required>
            <?php if (isset($errores['apellido_paterno'])): ?>
				<p class="error"><?php echo $errores['apellido_paterno'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="ap_mat">Apellido Materno:</label>
			<input type="text" name="persona[apellido_materno]" id="ap_mat" placeholder="Segundo Apellido" value="<?php echo $persona->apellido_materno; ?>" required>
            <?php if (isset($errores['apellido_materno'])): ?>
				<p class="error"><?php echo $errores['apellido_materno'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="edad">Edad:</label>
			<input type="number" name="persona[edad]" id="edad" placeholder="Tu edad" value="<?php echo $persona->edad; ?>" required>
            <?php if (isset($errores['edad'])): ?>
				<p class="error"><?php echo $errores['edad'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="genero">Genero:</label>
			<select name="persona[id_genero]" id="genero" required>
				<option value="">-- Selecciona --</option>
				<?php foreach ($generos as $genero): ?>
				<option value="<?php echo $genero->id ?>" <?php echo ($persona->id_genero === $genero->id) ? 'selected' : '' ?>><?php echo $genero->genero ?></option>
				<?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_genero'])): ?>
				<p class="error"><?php echo $errores['id_genero'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="curp">Curp:</label>
			<input type="text" name="persona[curp]" id="curp" placeholder="ANMQ634092XZRFBH28 (18 digitos)" value="<?php echo $persona->curp; ?>" required>
            <?php if (isset($errores['curp'])): ?>
				<p class="error"><?php echo $errores['curp'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="rfc">RFC:</label>
			<input type="text" name="persona[rfc]" id="rfc" placeholder="WFIS7741128H0 (13 digitos)" value="<?php echo $persona->rfc; ?>" required>
            <?php if (isset($errores['rfc'])): ?>
				<p class="error"><?php echo $errores['rfc'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="nss">NSS:</label>
			<input type="text" name="persona[nss]" id="" placeholder="10609657163 (11 digitos)" value="<?php echo $persona->nss; ?>" required>
            <?php if (isset($errores['nss'])): ?>
				<p class="error"><?php echo $errores['nss'] ?></p>
            <?php endif; ?>
		</div>
	</div>
	<div class="columna-formulario">
		<div class="campo-formulario">
			<label for="Cédula">Cédula Profesional:</label>
			<input type="text" name="persona[cedula]" id="" placeholder="14305415 (8 digitos)" value="<?php echo $persona->cedula; ?>" required>
            <?php if (isset($errores['cedula'])): ?>
				<p class="error"><?php echo $errores['cedula'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="fech_nac">Fecha de Nacimiento:</label>
			<input type="date" name="persona[fecha_nacimiento]" id="fech_nac" placeholder="13/10/2000" value="<?php echo $persona->fecha_nacimiento; ?>" required>
            <?php if (isset($errores['fecha_nacimiento'])): ?>
				<p class="error"><?php echo $errores['fecha_nacimiento'] ?></p>
            <?php endif; ?>
		</div>

		<div class="campo-formulario">
			<label for="nacionalidad">Nacionalidad:</label>
			<select name="persona[id_nacionalidad]" id="nacionalidad" required>
				<option value="">-- Selecciona --</option>
                <?php foreach ($nacionalidades as $nacionalidad): ?>
					<option value="<?php echo $nacionalidad->id ?>" <?php echo ($persona->id_nacionalidad === $nacionalidad->id) ? 'selected' : '' ?>><?php echo $nacionalidad->nacionalidad ?></option>
                <?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_nacionalidad'])): ?>
				<p class="error"><?php echo $errores['id_nacionalidad'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="estado_civil">Estado Civil:</label>
			<select name="persona[id_estado_civil]" id="estado_civil" required>
				<option value="">-- Selecciona --</option>
                <?php foreach ($estado_civil as $est_civil): ?>
					<option value="<?php echo $est_civil->id ?>" <?php echo ($persona->id_estado_civil === $est_civil->id) ? 'selected' : '' ?>><?php echo $est_civil->estado_civil ?></option>
                <?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_estado_civil'])): ?>
				<p class="error"><?php echo $errores['id_estado_civil'] ?></p>
            <?php endif; ?>
		</div>

		<div class="campo-formulario">
			<label for="compania">Compañia:</label>
			<select name="persona[id_compania]" id="compania" required>
				<option value="">-- Selecciona --</option>
                <?php foreach ($companias as $compania): ?>
					<option value="<?php echo $compania->id ?>" <?php echo ($persona->id_compania === $compania->id) ? 'selected' : '' ?>><?php echo $compania->compania ?></option>
                <?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_compania'])): ?>
				<p class="error"><?php echo $errores['id_compania'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="profesion">Profesión:</label>
			<select name="persona[id_profesion]" id="profesion" required>
				<option value="">-- Selecciona --</option>
                <?php foreach ($profesiones as $profesion): ?>
					<option value="<?php echo $profesion->id ?>" <?php echo ($persona->id_profesion === $profesion->id) ? 'selected' : '' ?>><?php echo $profesion->profesion ?></option>
                <?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_profesion'])): ?>
				<p class="error"><?php echo $errores['id_profesion'] ?></p>
            <?php endif; ?>
		</div>
		<div class="campo-formulario">
			<label for="idioma">Idioma:</label>
			<select name="persona[id_idioma]" id="idioma" required>
				<option value="">-- Selecciona --</option>
                <?php foreach ($idiomas as $idioma): ?>
					<option value="<?php echo $idioma->id ?>" <?php echo ($persona->id_idioma === $idioma->id) ? 'selected' : '' ?>><?php echo $idioma->idioma ?></option>
                <?php endforeach; ?>
			</select>
            <?php if (isset($errores['id_idioma'])): ?>
				<p class="error"><?php echo $errores['id_idioma'] ?></p>
            <?php endif; ?>
		</div>
	</div>
</div>
