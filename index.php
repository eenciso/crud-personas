<?php

use Models\Personas;

require 'includes/app.php';

// Recupera la información para mostrar el mensaje de las transacciones
$resultado = $_GET['resultado'] ?? null;
$accion = $_GET['accion'] ?? null;

if (!is_null($resultado) && !is_null($accion)) {
    $mensaje = mensaje($resultado, $accion);
}


/*
 * Devuelve los registros en un array de objetos
 * -> La clase Personas tiene un require asignado en app.php no es necesario volver a importarla
 * -> Tiene autoload
 */
$personas = Personas::all();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id'];
    // Busca el registro y crea una instancia de la clase
    $persona = Personas::find($id);
    // Borra el registro previamente instanciado
    $persona->delete();
}

layout('header');
?>

	<div class="contenedor">
		<div class="acciones">
			<a href="/admin/personas/crear.php" class="btn">Registrar Persona</a>
		</div>
        <?php if (isset($mensaje)): ?>
			<div class="<?php echo $mensaje['clases'] ?>">
				<p><?php echo $mensaje['mensaje'] ?></p>
			</div>
        <?php endif; ?>
		<table class="personas">
			<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Género</th>
				<th>Profesión</th>
				<th>Compañia</th>
				<th>Acciones</th>
			</tr>
			</thead>
			<tbody>
            <?php
            foreach ($personas as $persona):
                ?>
				<tr>
					<td><?php echo $persona->id ?></td>
					<td><?php echo $persona->nombre . ' ' . $persona->apellido_paterno ?></td>
					<td><?php echo $persona->genero ?></td>
					<td><?php echo $persona->profesion ?></td>
					<td><?php echo $persona->compania ?></td>
					<td>
						<a href="/admin/personas/actualizar.php?id=<?php echo $persona->id ?>" class="btn">
							<svg class="editar-persona" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path fill="" d="M224 256c70.7 0 128-57.3 128-128C352 57.3 294.7 0 224 0 153.3 0 96 57.3 96 128c0 70.7 57.3 128 128 128Zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16 -26 0-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h274.9c-2.4-6.8-3.4-14-2.6-21.3l6.8-60.9 1.2-11.1 7.9-7.9 77.3-77.3c-24.5-27.7-60-45.5-99.9-45.5Zm45.3 145.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9 -71.7-71.7 -137.9 137.8ZM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-37.8 37.8 -4.1 4.1 71.8 71.7 41.8-41.8c9.3-9.4 9.3-24.5 0-33.9Z" transform="matrix(.037 0 0 .037 0 2.4)"/></svg>
						</a>
						<form action="" method="post">
							<input type="hidden" name="id" value="<?php echo $persona->id ?>">
							<button type="submit" class="btn">
								<svg class="borrar-persona" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><g transform="scale(.046)"><g fill="none"><path d="M296 64h-80v0c-4.37-.05-7.96 3.45-8 7.81 -.01.06-.01.12 0 .18v24h96v-24 0c.04-4.37-3.46-7.96-7.82-8 -.07-.01-.13-.01-.19 0Z"/><path d="M292 64h-72v0c-2.21 0-4 1.79-4 4v28h80V68v0c0-2.21-1.8-4-4-4Z"/></g><path d="M447.55 96H336V48v0c0-8.84-7.17-16-16-16H192v0c-8.84 0-16 7.16-16 16v48H64.45L64 136h33l20.09 314v0c1.05 16.85 15.02 29.98 31.91 30h214v-.01c16.87-.01 30.84-13.11 31.93-29.95L415 135.981h33ZM176 416l-9-256h33l9 256Zm96 0h-32V160h32Zm24-320h-80V68v0c0-2.21 1.79-4 4-4h72v0c2.2 0 4 1.79 4 4Zm40 320h-33l9-256h33Z"/></g></svg>
							</button>
						</form>
					</td>
				</tr>
            <?php endforeach; ?>
			</tbody>
		</table>
	</div>

<?php
layout('footer');
?>