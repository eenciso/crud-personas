<?php

namespace Models;

use PDO;

class Personas
{
    public static $dbh;
    public static array $errores = [];

    public $id;
    public string $nombre;
    public string $apellido_paterno;
    public string $apellido_materno;
    public string $edad;
    public $id_genero;
    public string $curp;
    public string $rfc;
    public string $nss;
    public string $cedula;
    public string $fecha_nacimiento;
    public string $fecha_registro;
    public $id_nacionalidad;
    public $id_estado_civil;
    public $id_compania;
    public $id_profesion;
    public $id_idioma;

    public function __construct($args = [])
    {
        // Se puede validar con operador ternario
        $this->id = $args['id'] ?? null;
        $this->nombre = $args['nombre'] ?? '';
        $this->apellido_paterno = $args['apellido_paterno'] ?? '';
        $this->apellido_materno = $args['apellido_materno'] ?? '';
        $this->edad = $args['edad'] ?? '';
        $this->id_genero = $args['id_genero'] ?? null;
        $this->curp = $args['curp'] ?? '';
        $this->rfc = $args['rfc'] ?? '';
        $this->nss = $args['nss'] ?? '';
        $this->cedula = $args['cedula'] ?? '';
        $this->fecha_nacimiento = $args['fecha_nacimiento'] ?? '';
        $this->fecha_registro = $args['fecha_registro'] ?? date('Y-m-d');
        $this->id_nacionalidad = $args['id_nacionalidad'] ?? null;
        $this->id_estado_civil = $args['id_estado_civil'] ?? null;
        $this->id_compania = $args['id_compania'] ?? null;
        $this->id_profesion = $args['id_profesion'] ?? null;
        $this->id_idioma = $args['id_idioma'] ?? null;
    }

    // Asigna la conexión de la BD
    public static function setDB($dbh)
    {
        self::$dbh = $dbh;
    }

    // Consulta todos los registros de la clase personas
    public static function all()
    {
        $sql = "SELECT pr.id, pr.nombre, pr.apellido_paterno, pr.apellido_materno, g.genero, p.profesion, c.compania
        FROM personas pr LEFT JOIN generos g on g.id = pr.id_genero
        LEFT JOIN profesiones p on p.id = pr.id_profesion
        LEFT JOIN companias c on c.id = pr.id_compania
        ORDER BY id";

        return self::consultarSQL($sql);
    }

    // Guarda un nuevo registro en la base de datos (¡requiere instancia de la clase!)
    public function save()
    {
        $sql = "INSERT INTO personas VALUES 
        (:id, :nombre, :apellido_paterno, :apellido_materno, :edad, :id_genero, :curp, :rfc, :nss, :cedula, :fecha_nacimiento, 
         :fecha_registro, :id_nacionalidad, :id_estado_civil, :id_compania, :id_profesion, :id_idioma)";

        // Se le indica a la base que se prepare para ejecutar la consulta
        $stmt = self::$dbh->prepare($sql);

        // Inserción de objetos directamente en la base de datos, ya que las propiedades coinciden con los nombres de las variables de la clase
        $resultado = $stmt->execute((array)$this);

        // Cerramos la conexión
        self::$dbh = null;

        // Evalua el resultado y lo pasa a redireccionar junto con el tipo de transacción
        if ($resultado) {
            redireccionar($resultado, 'save');
        }
    }

    // Actualiza los datos de un registro en la base de datos (¡requiere instancia de la clase!)
    public function update()
    {
        $sql = "UPDATE personas SET id=:id, nombre=:nombre, apellido_paterno=:apellido_paterno, 
                apellido_materno=:apellido_materno, edad=:edad, id_genero=:id_genero, curp=:curp, 
                rfc=:rfc, nss=:nss, cedula=:cedula, fecha_nacimiento=:fecha_nacimiento, fecha_registro=:fecha_registro, 
                id_nacionalidad=:id_nacionalidad, id_estado_civil=:id_estado_civil, id_compania=:id_compania, 
                id_profesion=:id_profesion, id_idioma=:id_idioma WHERE id=:id";
        $stmt = self::$dbh->prepare($sql);
        // Se envía la referencia a la clase con las propiedades previamente sincronizadas en memoria
        $resultado = $stmt->execute((array)$this);
        // Cierra la conexión
        self::$dbh = null;
        // Evalua el resultado y lo pasa a redireccionar junto con el tipo de transacción
        if ($resultado) {
            redireccionar($resultado, 'update');
        }
    }

    // Borra un registro en la base de datos (¡requiere instancia de la clase!)
    public function delete()
    {
        $sql = "DELETE FROM personas WHERE id = :id";
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);// Solo se inserta el dato una vez
        $resultado = $stmt->execute();
        // Cierra la conexion
        self::$dbh = null;
        // Evalua el resultado y lo pasa a redireccionar junto con el tipo de transacción
        if ($resultado) {
            redireccionar($resultado, 'delete');
        }
    }

    // Busca un registro en la base de datos por su id

    /**
     * Mapea los datos del registro en un objeto de la clase Persona
     * Cuando NO SE UTILIZA autoload se hace referencia a la clase con un string,
     * Cuando sí basta con un self o this dependiendo si la clase esta instanciada o no, o con el nombre de la clase y ::
     * @param $id
     * @return mixed
     *
     */
    public static function find($id): mixed
    {
        $sql = "SELECT * FROM personas WHERE id = :id";
        $stmt = self::$dbh->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Personas::class, [[]]);// ::class - Nombre de clase completo = Personas::class o Models\Personas
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);// Solo se inserta el dato una vez
        $stmt->execute();

        return $stmt->fetch();
    }

    public function validarPersona(): array
    {
        if (!$this->nombre) {
            self::$errores['nombre'] = 'Ingresa un nombre';
        }
        if (!$this->apellido_paterno) {
            self::$errores['apellido_paterno'] = 'Ingresa el primer apellido';
        }
        if (!$this->apellido_materno) {
            self::$errores['apellido_materno'] = 'Ingresa el segundo apellido';
        }
        if (!$this->edad) {
            self::$errores['edad'] = 'Ingresa una edad valida';
        }
        if (!$this->id_genero) {
            self::$errores['id_genero'] = 'Selecciona un genero';
        }
        if (!$this->curp || strlen($this->curp) < 18) {
            self::$errores['curp'] = 'Ingresa el CURP (18 dígitos)';
        }
        if (!$this->rfc || strlen($this->rfc) < 13) {
            self::$errores['rfc'] = 'Ingresa el RFC (13 dígitos)';
        }
        if (!$this->nss || strlen($this->nss) < 11) {
            self::$errores['nss'] = 'Ingresa el NSS (11 dígitos)';
        }
        if (!$this->cedula || strlen($this->cedula) < 8) {
            self::$errores['cedula'] = 'Ingresa el no. de Cédula (8 dígitos)';
        }
        if (!$this->fecha_nacimiento) {
            self::$errores['fecha_nacimiento'] = 'Elige la fecha de nacimiento';
        }
        if (!$this->id_nacionalidad) {
            self::$errores['id_nacionalidad'] = 'Elige una nacionalidad';
        }
        if (!$this->id_estado_civil) {
            self::$errores['id_estado_civil'] = 'Elige el estado civil';
        }
        if (!$this->id_compania) {
            self::$errores['id_compania'] = 'Elige la compañia';
        }
        if (!$this->id_profesion) {
            self::$errores['id_profesion'] = 'Elige una profesión';
        }
        if (!$this->id_idioma) {
            self::$errores['id_idioma'] = 'Elige un idioma';
        }

        return self::$errores;
    }

    // Ejecuta la sentencia SQL y devuelve un array de objetos
    private static function consultarSQL($sql)
    {
        $stmt = self::$dbh->prepare($sql);
        // Mapea los datos y los devuelve con formato de objeto (objeto anónimo)
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // Mapea el resultado de la consulta en la clase Personas (útil para actualizar registros)
    private static function mapearSQL($sql, $parametros)
    {
        $stmt = self::$dbh->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Personas', [[]]);
        $stmt->execute();

        return $stmt->fetch();
    }

    // Sincroniza los datos del objeto en memoria con los del array que recibe
    public function sincronizarDatos($nuevos_datos)
    {
        // Recorre el array de los nuevos datos y los asigna a las propiedades de la instancia
        foreach ($nuevos_datos as $key => $value) {
            if (property_exists($this, $key) && !is_null($value)) {
                $this->$key = $value;
            }
        }
    }

    // MÉTODOS PARA LLENAR SELECTS
    public static function getGeneros()
    {
        $sql = "SELECT * FROM generos";
        return self::consultarSQL($sql);
    }

    public static function getNacionalidades()
    {
        $sql = "SELECT * FROM nacionalidades";
        return self::consultarSQL($sql);
    }

    public static function getEstadoCivil()
    {
        $sql = "SELECT * FROM estado_civil";
        return self::consultarSQL($sql);
    }

    public static function getCompanias()
    {
        $sql = "SELECT * FROM companias";
        return self::consultarSQL($sql);
    }

    public static function getProfesiones()
    {
        $sql = "SELECT * FROM profesiones";
        return self::consultarSQL($sql);
    }

    public static function getIdiomas()
    {
        $sql = "SELECT * FROM idiomas";
        return self::consultarSQL($sql);
    }
}