<?php
require 'includes/app.php';

layout('header');
?>
	<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Personas JS</title>
		<link rel="stylesheet" href="/src/css/reset.min.css">
		<link rel="stylesheet" href="/src/css/styles.css">
	</head>
	<body>

		<div class="contenedor">
			<div class="acciones">
				<a href="/admin/personas/crear.php" class="btn">Registrar Persona</a>
			</div>

			<div class="contenedor">
				<form action="" method="post" class="formulario busqueda" id="buscar-persona">
					<div class="campo-formulario">
						<input type="text" class="" placeholder="nombre..." id="inputBusqueda" autocomplete="off">
					</div>
					<input type="submit" value="Buscar">
				</form>
			</div>

			<h1>Personas</h1>
			<table class="personas">
				<thead>
				<tr>
					<th class="col" id="col-id">Id</th>
					<th class="col" id="col-nombre">Nombre</th>
					<th class="col" id="col-genero">Género</th>
					<th class="col" id="col-profesion">Profesión</th>
					<th class="col" id="col-compania">Compañia</th>
					<th>Acciones</th>
				</tr>
				</thead>
				<tbody id="body-personas">
				</tbody>
			</table>
		</div>
		<script src="/src/js/app.js"></script>
	</body>
	</html>
<?php
layout('footer');
?>