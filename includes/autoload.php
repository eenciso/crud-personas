<?php

function autoload_class($clase)
{
//    echo $clase;
    // Quitar el namespace de la clase
    $nombre_clase = explode('\\', $clase);
//    var_dump($nombre_clase[1]);
    // Ruta exacta desde donde se llamaran las clases a importar
    require __DIR__ . '/../models/' . $nombre_clase[1] . '.php';
}
spl_autoload_register('autoload_class');