<?php

const TEMPLATES_URL = __DIR__ . '/templates';

function debug($cod)
{
    echo "<pre>";
    var_dump($cod);
    echo "</pre>";
    exit;
}

function layout($layout)
{
    include TEMPLATES_URL . "/${layout}.php";
}

function redireccionar($resultado, $accion)
{
    header('Location: /?resultado=' . $resultado . '&accion=' . $accion);
}

function mensaje($resultado, $accion): array
{
    $info_mensaje = [];

    if ($resultado) {
        switch ($accion) {
            case 'save':
                $info_mensaje['clases'] = 'alerta alerta-exito';
                $info_mensaje['mensaje'] = 'Registro creado correctamente';
                break;
            case 'update':
                $info_mensaje['clases'] = 'alerta alerta-info';
                $info_mensaje['mensaje'] = 'Registro actualizado correctamente';
                break;
            case 'delete':
                $info_mensaje['clases'] = 'alerta alerta-error';
                $info_mensaje['mensaje'] = 'Registro eliminado correctamente';
                break;
        }
    } elseif (!$resultado) {
        $info_mensaje['clases'] = 'alerta alerta-error';
        $info_mensaje['mensaje'] = 'Ocurrió un error!';
    }

    return $info_mensaje;
}