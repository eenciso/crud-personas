<?php

use Models\Personas;

require 'funciones.php';
require 'database/conexion.php';
require 'autoload.php';

// Importando la clase Personas o se puede utilizar el autoload
//require __DIR__ . '/../models/Personas.php';

// Conectar la base de datos
$db = conectarBD();

// Asignando BD a la clase persona
Personas::setDB($db);

