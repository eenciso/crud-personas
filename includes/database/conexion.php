<?php

function conectarBD()
{
    $dbh = null;
    try {
        $dbh = new PDO('mysql:host=localhost; dbname=personas', 'root', '');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    return $dbh;
}
